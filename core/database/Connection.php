<?php

namespace App\Core\Database;

use PDO;
use PDOException;

class Connection
{
    /**
     * Create a new PDO connection.
     *
     * @param array $config
     */
    public static function make($config)
    {
        try {
            return new PDO(
               'sqlite:'.$config['sqlite']
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
