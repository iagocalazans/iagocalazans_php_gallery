===========================


PHP STANDARDS DEVELOPMENT
================


===========================


 Challenge 
================



Crie uma aplicação web onde seja possível cadastrar uma nova foto, ver a lista de fotos cadastradas e excluir uma foto.


Nessa aplicação, faça uma página que exiba todas as fotos cadastradas em uma galeria como a imagem em anexo `todas-fotos.jpg`.



=========================== 


Rules
================



- Criar uma página para listar as fotos cadastradas. Esta listagem deve conter um link para remover a imagem do banco de dados e do disco.


- Criar uma página para para cadastrar uma nova foto
.

- Criar uma página para exibir as fotos em formato de galeria. (Fique livre para utilizar qualquer framework css/js para frontend)



=========================== 


Requisites
=================



- Use o PHP 7+ e SQLight para armazenar as informações
.

- Considere que a aplicação será executada utilizando servidor. [built-in do PHP](http://php.net/manual/pt_BR/features.commandline.webserver.php)


- Procure não utilizar outras ferramentas como frameworks e/ou componentes externos.


- Utilize o Composer para fazer o autoloader das classes.


- Crie um arquivo (README.md) contendo documentação da aplicação bem como um roteiro para instalação do programa.

=========================== 

 Whats going on
====================

- Organização do Código


- Capacidade de abstração do problema


- Teste Unitário


- Documentação