<?php require('partials/head.php'); ?>

    <h1>All Images</h1>

<?php foreach ($images as $img) : ?>
	<ul>
	<form method="POST" action="images/remove">
		<input type="hidden" name="id" value="<?= $img->id; ?>">
		<input type="hidden" name="path" value="<?= $img->path; ?>">
	    <img src="<?= $img->path; ?>" />
	    <?= $img->desc; ?>
	    <button type="submit">Remover</button>
    </form>
    </ul>
<?php endforeach; ?>

<h1>Submit Your Image</h1>

<form enctype="multipart/form-data" method="POST" action="images">
    Enviar esse arquivo: <input name="userfile" type="file" />
    Descreva sua imagem: <textarea name="desc"></textarea>
    <button type="submit">Enviar</button>
</form>

<?php require('partials/footer.php'); ?>
