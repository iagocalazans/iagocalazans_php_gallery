 <?php

$router->get('', 'PagesController@home');
$router->get('about', 'PagesController@about');
$router->get('contact', 'PagesController@contact');

$router->get('images', 'ImagesController@index');
$router->post('images', 'ImagesController@upload');
$router->post('images/remove', 'ImagesController@remove');