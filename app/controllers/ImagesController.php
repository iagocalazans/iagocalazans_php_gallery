<?php

namespace App\Controllers;

use App\Core\App;

class ImagesController
{
    /**
     * Show all images.
     */
    public function index()
    {
        $images = App::get('database')->selectAll('images');

        return view('images', compact('images'));
    }

    /**
     * Store a new image in the database.
     */
    public function upload()
    {

        $uploaddir = 'public/uploads/';

        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            
            App::get('database')->insert('images', [
                'name' => $_FILES['userfile']['name'],
                'desc' => $_POST['desc'],
                'path' => $uploadfile
            ]);
            var_dump($_FILES['userfile']);
            echo "Arquivo válido e enviado com sucesso.\n";
        } else {
            print_r(error_get_last());
        }
        
    }


    /**
     * Remove a image from the database.
     */
    public function remove()
    {

        App::get('database')->delete('images', [
            'id' => $_POST['id']
        ]);

        if (unlink($_POST['path'])) {
            echo "Deu certo!";
        } else {
            echo "Falhou!";
        }
    }
}
